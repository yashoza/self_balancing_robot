%% Load parameters
bot_para;
%% variables
alpha = n * Kt / Rm;
beta = n * Kt * Kb / Rm + fm;
E = [(2*m+M)*R^2 + 2*Jw + 2*n^2*Jm   M*L*R - 2*n^2*Jm;
      M*L*R - 2*n^2*Jm               M*L^2 + Jpsi + 2*n^2*Jm];
F = 2*[beta+fw   -beta; 
       -beta     beta];
G = [0   0; 
     0  -M*g*L];
H = [alpha   alpha; 
    -alpha  -alpha];
I = m * W^2 / 2 + Jphi + (Jw + n^2 * Jm) * W^2 / (2 * R^2);
J = W^2 / (2 * R^2) * (beta + fw);
K = W / (2 * R) * alpha;
%% State space matrices
A1 = [0 0   1 0;
	  0 0   0 1;
     -E^-1*G  -E^-1*F]; 
B1 = [0 0;
	  0 0;
	  E^-1*H];
C1 = eye(4);
D1 = zeros(4, 2);
A2 = [0   1
	  0  -J/I];
B2 = [0    0
	 -K/I  K/I];
C2 = eye(2);
D2 = zeros(2);
%% State space models
s1 = ss(A1, B1, C1, D1);
s1.StateName = {'theta', 'psi', 'thetadot', 'psidot'};
s1.InputName = {'Vl', 'Vr'};
s1.OutputName = {'theta', 'psi', 'thetadot', 'psidot'};
%%Integrator
s0=ss(1/tf('s'));
s0.StateName='thetaint';
s0.OutputName='thetaint';
s3=append(s0,s1);% REFER CALCULATION DONE IN THE NOTEBOOK AND JULIO PPT
s3.A(1,2)=-1;
s3(:,1)=[];
%%
s2 = ss(A2, B2, C2, D2);
s2.StateName = {'phi', 'phidot'};
s2.InputName = {'Vl', 'Vr'};
s2.OutputName = {'phi', 'phidot'};
s4=append(s3,s2); %append two statespace
s4.B(end,[1,2])=s4.B(end,[3,4]); %copying 2 values of phidot which was in 3rd and 4th column to 
                                 %first and second column
s4(:,[3,4])=[]; %Deleting the 3rd and 4th column from B and D
%%