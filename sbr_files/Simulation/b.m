clc;
clear all;

A=[0 1 0 0;0 -0.0097 11.1594 0;0 0 0 1;0 -0.0293 172.1160 0];%From SS equation
B=[0;0.0815;0;0.2456];
C=[1 0 0 0;0 0 1 0];
D=[0; 0];
sys_ss= ss(A,B,C,D);


poles= eig(A);% Actual poles of system
P = [-2 + sqrt(3)*2j;-2 - sqrt(3)*2j;-1;-40];% placed poles of system
K=acker(A,B,P);
X=[5;2;30;-40]; % Here we set the initial condition.


thetamatrix=zeros(400,1);
xmatrix=zeros(400,1);
myworld=vrworld('Self3D.wrl');
open(myworld);
Body.body=vrnode(myworld,'WheelBody');
view(myworld);


for i=1:400
    Xdot=(A - B*K)*X;
    X = X + 0.01*Xdot;  % performing numerical calculation for solving x
                        % with 0.01 as the interval
                        % since u=-Kx; we find x by numerical methods and
                        % plug it in the equation.
    thetamatrix(i)=deg2rad(X(3));
    xmatrix(i)=X(1);
    Body.body.rotation=[ 1 0 0 thetamatrix(i)];
    Body.body.translation=[ 0 xmatrix(i) 2];
    pause(0.01); 
end
t=0:0.01:3.99;
figure(1)
plot(transpose(t),thetamatrix);
figure(2)
plot(transpose(t),xmatrix);
