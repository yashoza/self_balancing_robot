
clc;
clear all;

A=[0 1 0 0;0 -0.0097 11.1594 0;0 0 0 1;0 -0.0293 172.1160 0];%From SS equation
B=[0;0.0815;0;0.2456];
C=[1 0 0 0;0 0 1 0];
D=[0; 0];
sys_ss= ss(A,B,C,D);
co=ctrb(sys_ss);%[B|A*B|A^2*B|A^3*B]
controllability=rank(co);
%LQR design simplest case assume R=1 and Q=C'C and change the Q value after
%each simulation to give the desired effect
Q=C'*C;
Q(1,1)=6000; % first run without these two lines and see the result,
Q(3,3)=100;  % then increase the values
R=1;
K=lqr(A,B,Q,R);
Ac=[(A-B*K)];
Bc=[B];
Cc=[C];
Dc=[D];
poles_cl=eig(Ac);




X=[2;1;10;-10]; % Here we set the initial condition.


thetamatrix=zeros(1000,1);
xmatrix=zeros(1000,1);
myworld=vrworld('Self3D.wrl');
open(myworld);
Body.body=vrnode(myworld,'WheelBody');
view(myworld);


for i=1:1000
    
    Xdot=(A - B*K)*X;
    
    X = X + 0.01*Xdot;  % performing numerical calculation for solving x
                      % with 0.01 as the interval
                        %since u=-Kx; we find x by numerical methods and
                        %plug it in the equation.
    
    thetamatrix(i)=deg2rad(X(3));
    xmatrix(i)=X(1);
    Body.body.rotation=[ 1 0 0 thetamatrix(i)];
    Body.body.translation=[ 0 xmatrix(i) 2];
    pause(0.01); 
end
t=0:0.01:9.99;
figure(1)
plot(transpose(t),rad2deg(thetamatrix));
xlabel('t'), ylabel('theta')
figure(2)
plot(transpose(t),xmatrix);
xlabel('t'), ylabel('x')
