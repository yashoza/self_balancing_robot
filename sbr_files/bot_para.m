%% constants
g = 9.81;                       % gravity
%%parameters
m = 0.024;						% wheel wt [kg]
R = 0.027;						% wheel rad [m]
Jw = m * R^2 / 2;				% wheel MOI [kgm^2]
M = 0.8;                        % body wt [kg]
W = 0.105;						% body wd [m]
D = 0.1;						% body dp [m]
h = 0.21;						% body ht [m]
L = h / 2;						% distance of the COM from the wheel axle [m]
Jpsi = M * L^2 / 3;				% body pitch MOI [kgm^2]
Jphi = M * (W^2 + D^2) / 12;	% body yaw MOI [kgm^2]
fm = 0.0022;					% friction coefficient between body & DC motor
fw = 0;           				% friction coefficient between wheel & floor
%% Motor
Jm = 1e-5;						% DC motor inertia [kgm^2]
Rm = 6.69;						% DC motor resistance [Ohm]
Kb = 0.468;						% DC motor back EMF constant [Vsec/rad]
Kt = 0.317;						% DC motor torque constant [Nm/A]
n = 1;							% Gear ratio
K_PWM = 8.087;                  % Battery voltage level

%%
psi_in = 0.01;                    % Initial value
Ts=0.004;